# Dev_Ops

This repo freely follows the OpenClassrooms course on  [DevOps with Gitlab](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/)

Also uses extensively this Medium tutorial on [how to use Nginx with Streamlit](https://medium.com/@dasirra/using-streamlit-nginx-docker-to-build-and-put-in-production-dashboards-in-aws-lightsail-781dab8f2836) 

The app is available at [this adress](http://18.190.74.150/app) !

Thanks for checking out my work :)
