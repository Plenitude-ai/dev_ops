#!bin/bash
docker stop $(docker ps --all --quiet)
docker rm $(docker ps --all --quiet)

docker compose down

docker compose pull

docker compose up --build