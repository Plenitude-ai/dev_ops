import os
import json
import time
import datetime
import requests
from frontend_utils import ROOT_PDF_ENDPOINTS

import frontend_utils as utils

import streamlit as st
# Perfect streamlit tuto :
# https://codingfordata.com/8-simple-and-useful-streamlit-tricks-you-should-know/

# # how to handle cookies in Streamlit
# https://pypi.org/project/extra-streamlit-components/
# import extra_streamlit_components as stx



file_size = 0

# To be able to have the logs, the log.warnings in the running container
import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


if __name__ == '__main__':

    st.header("App Scanned PDF enhancement")

    st.markdown("____")
    st.subheader("File Upload")


    uploaded_file = st.file_uploader('Choose your .pdf file', type="pdf")
    if uploaded_file is not None:
        log.warning("\n\n\n\n\nType(uploaded_file) : %s", type(uploaded_file))
        log.warning("\n\nUploaded_file.name : %s", uploaded_file.name)
        log.warning("\n\nUploaded_file.size : %s", uploaded_file.size)
        file_size = uploaded_file.size
        log.warning("file_size: %s", file_size)

    st.write("File Size : ", str(file_size)+"Bits")

    st.markdown("____")
    st.subheader("Job config")

    # Docs for streamlit forms : https://blog.streamlit.io/introducing-submit-button-and-forms/
    with st.form(key='my_form'):
        job_config = st.columns(4)
        upsampling_factor = job_config[0].radio("Upsamling factor : ", [4, 3, 2, None])
        deskew = job_config[1].radio("Deskewing : ", [True, False])
        binarize = job_config[2].radio("Binarization : ", [True, False])
        language = job_config[3].text_input("OCR params, language : ", "english")

    
        email_address = st.text_input("Email to send the result to : ", "axeldurandfontanel@hotmail.fr")
        log.warning("\n\n\n email_address : %s", email_address)

        submit_button = st.form_submit_button(label='Submit')
        log.warning("submit_button : %s", submit_button)
    

    # ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ---------- 
    # ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ---------- 
    # ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ----------   ---------- LAUNCHING THE TASK ----------   
    if submit_button == True:
        log.warning("\n\nSTART OF THE SCRIPT !!!")
        log.warning("submit_button : %s", submit_button)

        #Checks on email_address
        if type(email_address) != str:
            raise TypeError('email_address should be str, but was {}'.format(type(email_address)))
        if "@" not in email_address:
            raise TypeError("email_address should contain character @ at least, you passed : {}".format(email_address))
        if len(email_address) <=5:
            raise TypeError("email_address should contain 5 characters at least, you passed : {}".format(email_address))


        # Builds job_config
        job_config = {"deskew": deskew,
                "binarize": binarize,
                "super_resolution": {"upsampling_factor": upsampling_factor},
                "ocr_params":{"language":language}
            }
        log.warning("job_config : %s", job_config)



        # PUSHING THE METADATA AND BYTES
        # Pushing Metadata to Pg-SQL DB
        # Also pushes the file to GCP Bucket
        log.warning("\nPushing the metadata...")
        r = utils.pushes_metadata_and_file_to_databases(job_config, uploaded_file)
        
        id_ = int(r.text)
        if type(id_) != int:
            raise Exception("r.text is not int")


        # LAUNCHING TASK
        log.warning("\nLaunching the task...")
        st.write('Launching the task...' )
        task_name = "run_enhancer"
        r = requests.post(ROOT_PDF_ENDPOINTS+"/launch_task"+f"/{task_name}")
        log.warning("r.status_code : %s", r.status_code)
        if r.status_code != 201:
            log.warning("\nERROR IN LAUNCHING TASK : PLEASE USE 'run_enhancer' or 'run_extractor'")



        # BUFFFERING TASK
        log.warning("\n\n\nBuffering task...")
        st.write("Task launched with id : {}.  \nWaiting for task to finish...".format(id_))
        log.warning("id_ : %s", id_)

        start, delta = time.time(), 0

        def get_status(id_):    
            r = requests.get(f"{utils.ROOT_PDF_ENDPOINTS}/pdf_db/{id_}")
            log.warning("\nr.text : %s", r.text)
            status = json.loads(r.text)['status']
            log.warning(f"The status of pdf {id_} is: {status}")
            return status

        time.sleep(5)
        status = get_status(id_)
        while (status=="pending" and delta < 180):
            time.sleep(5)
            delta = time.time() - start
            status = get_status(id_)
        log.warning("delta = %s", str(delta))
        if delta >=180:
            log.warning("task_finished_callback : Computing exceeded 180sc...")
            raise Exception("Computing exceeded 180sc...")

        st.write('The processing of pdf {} is over and will be sent out by mail !'.format(str(id_)) )



        # SENDING MAIL
        log.warning("\n\n\nSending_mail_callback...")
        log.warning("id_ : %s", id_)
        log.warning("email_address : %s", email_address)
        log.warning("filename : %s", uploaded_file.name)

        r = requests.post(ROOT_PDF_ENDPOINTS+"/send_mail", json={   "id_file":id_,\
                                                                "recipient_email":email_address,\
                                                                "original_filename":uploaded_file.name\
                                                            })
        st.write("Mail sent !")


